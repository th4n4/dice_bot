import os
import telebot
import re
import requests
import random

BOT_TOKEN = '6950556451:AAFz4sPeOsBJi3K9-ZIEZEjSWZmL5ZfRvLw'

bot = telebot.TeleBot(BOT_TOKEN)

def roll_dice(ndk):
    # Split the ndk string into n and k
    n, k = map(int, ndk.split('d'))
    # Roll the dice n times and collect the results
    rolls = [random.randint(1, k) for _ in range(n)]
    # Return the list of rolls
    return rolls

def apply_roll_dice(dice_list):
    # Iterate over the list of tuples
    raw_results = [(sign, roll_dice(ndk)) for sign, ndk in dice_list]
    # Return the new list with the results
    return raw_results

dice_list = [('+', '3d5'), ('-', '3d4'), ('+', '3d20')]
raw_results = apply_roll_dice(dice_list)
print(raw_results)

def process_raw_rolls(raw_results):
    for foo in raw_results:
        print(foo)

process_raw_rolls(raw_results)


@bot.message_handler(commands=['start', 'hello', 'help'])
def send_welcome(message):
    bot.reply_to(message, "Here we will have our help message")


@bot.message_handler(commands=['roll'])
def roll_dice(message):
    # Extract the parameters from the message text
    command_text = message.text
    # Regular expression to match standard dice notation
    dice_pattern = r"([+-]?\d+d\d+)"
    # Find all matches in the command text
    dice_matches = re.findall(dice_pattern, command_text)
    # Process the matches to get the desired list format
    # If no sign is specified, assume it's positive
    dice_list = [(match[0] if match[0] in '+-' else '+', match[1:]) for match in dice_matches]
    # Reply with the processed list
    bot.reply_to(message, str(dice_list))

#bot.infinity_polling()